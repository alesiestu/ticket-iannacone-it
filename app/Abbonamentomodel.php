<?php

  

namespace App;

  

use Illuminate\Database\Eloquent\Model;


   

class Abbonamentomodel extends Model

{

    protected $fillable = [

        'nome', 'tipo', 'costo', 'utente', 'scadenza'

    ];

}