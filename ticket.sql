-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Mag 30, 2019 alle 16:35
-- Versione del server: 10.1.37-MariaDB
-- Versione PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ticket`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `abbonamentomodels`
--

CREATE TABLE `abbonamentomodels` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `costo` text COLLATE utf8_unicode_ci NOT NULL,
  `utente` text COLLATE utf8_unicode_ci NOT NULL,
  `scadenza` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `abbonamentomodels`
--

INSERT INTO `abbonamentomodels` (`id`, `created_at`, `updated_at`, `tipo`, `costo`, `utente`, `scadenza`) VALUES
(1, NULL, NULL, 'Support google analytics', '50 €', 'geniushoroscope.com', '8-06-2019'),
(2, NULL, NULL, 'test', '50€', 'alessandro', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_07_22_115516_create_ticketit_tables', 2),
(4, '2015_07_22_123254_alter_users_table', 2),
(5, '2015_09_29_123456_add_completed_at_column_to_ticketit_table', 2),
(6, '2015_10_08_123457_create_settings_table', 2),
(7, '2016_01_15_002617_add_htmlcontent_to_ticketit_and_comments', 2),
(8, '2016_01_15_040207_enlarge_settings_columns', 2),
(9, '2016_01_15_120557_add_indexes', 2),
(10, '2019_05_30_132614_create_abbonamento_table', 3),
(11, '2019_05_30_132615_create_abbonamento_table', 4);

-- --------------------------------------------------------

--
-- Struttura della tabella `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `ticketit`
--

CREATE TABLE `ticketit` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `html` longtext COLLATE utf8_unicode_ci,
  `status_id` int(10) UNSIGNED NOT NULL,
  `priority_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `agent_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `completed_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `ticketit`
--

INSERT INTO `ticketit` (`id`, `subject`, `content`, `html`, `status_id`, `priority_id`, `user_id`, `agent_id`, `category_id`, `created_at`, `updated_at`, `completed_at`) VALUES
(3, 'test', 'test', '<p>test<br /></p>', 2, 2, 2, 1, 2, '2019-05-30 10:32:44', '2019-05-30 10:55:44', '2019-05-30 10:55:44'),
(4, 'Correzione testo', 'Testo piccolo articoli piu home', 'Testo piccolo articoli piu home<br /><p></p>', 1, 1, 3, 1, 2, '2019-05-30 12:19:51', '2019-05-30 12:20:30', NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `ticketit_audits`
--

CREATE TABLE `ticketit_audits` (
  `id` int(10) UNSIGNED NOT NULL,
  `operation` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ticket_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `ticketit_categories`
--

CREATE TABLE `ticketit_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `ticketit_categories`
--

INSERT INTO `ticketit_categories` (`id`, `name`, `color`) VALUES
(2, 'Assistenza tecnica', '#ff8000');

-- --------------------------------------------------------

--
-- Struttura della tabella `ticketit_categories_users`
--

CREATE TABLE `ticketit_categories_users` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `ticketit_categories_users`
--

INSERT INTO `ticketit_categories_users` (`category_id`, `user_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `ticketit_comments`
--

CREATE TABLE `ticketit_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `html` longtext COLLATE utf8_unicode_ci,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ticket_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `ticketit_comments`
--

INSERT INTO `ticketit_comments` (`id`, `content`, `html`, `user_id`, `ticket_id`, `created_at`, `updated_at`) VALUES
(1, 'pefetto', '<p>pefetto<br /></p>', 1, 1, '2019-05-30 10:00:23', '2019-05-30 10:00:23'),
(2, 'Fix in corso', '<p>Fix in corso<br /></p>', 1, 4, '2019-05-30 12:20:30', '2019-05-30 12:20:30');

-- --------------------------------------------------------

--
-- Struttura della tabella `ticketit_priorities`
--

CREATE TABLE `ticketit_priorities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `ticketit_priorities`
--

INSERT INTO `ticketit_priorities` (`id`, `name`, `color`) VALUES
(1, 'High', '#830909'),
(2, 'Normal', '#090909'),
(3, 'Low', '#125f71');

-- --------------------------------------------------------

--
-- Struttura della tabella `ticketit_settings`
--

CREATE TABLE `ticketit_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `default` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `ticketit_settings`
--

INSERT INTO `ticketit_settings` (`id`, `lang`, `slug`, `value`, `default`, `created_at`, `updated_at`) VALUES
(1, NULL, 'main_route', 'tickets', 'tickets', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(2, NULL, 'main_route_path', 'tickets', 'tickets', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(3, NULL, 'admin_route', 'tickets-admin', 'tickets-admin', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(4, NULL, 'admin_route_path', 'tickets-admin', 'tickets-admin', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(5, NULL, 'master_template', 'layouts.app', 'layouts.app', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(6, NULL, 'bootstrap_version', '4', '4', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(7, NULL, 'email.template', 'ticketit::emails.templates.ticketit', 'ticketit::emails.templates.ticketit', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(8, NULL, 'email.header', 'Ticket Update', 'Ticket Update', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(9, NULL, 'email.signoff', 'Thank you for your patience!', 'Thank you for your patience!', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(10, NULL, 'email.signature', 'Your friends', 'Your friends', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(11, NULL, 'email.dashboard', 'My Dashboard', 'My Dashboard', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(12, NULL, 'email.google_plus_link', '#', '#', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(13, NULL, 'email.facebook_link', '#', '#', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(14, NULL, 'email.twitter_link', '#', '#', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(15, NULL, 'email.footer', 'Powered by Ticketit', 'Powered by Ticketit', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(16, NULL, 'email.footer_link', 'https://github.com/thekordy/ticketit', 'https://github.com/thekordy/ticketit', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(17, NULL, 'email.color_body_bg', '#FFFFFF', '#FFFFFF', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(18, NULL, 'email.color_header_bg', '#44B7B7', '#44B7B7', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(19, NULL, 'email.color_content_bg', '#F46B45', '#F46B45', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(20, NULL, 'email.color_footer_bg', '#414141', '#414141', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(21, NULL, 'email.color_button_bg', '#AC4D2F', '#AC4D2F', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(22, NULL, 'default_status_id', '1', '1', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(23, NULL, 'default_close_status_id', '2', '0', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(24, NULL, 'default_reopen_status_id', '3', '0', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(25, NULL, 'paginate_items', '10', '10', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(26, NULL, 'length_menu', 'a:2:{i:0;a:3:{i:0;i:10;i:1;i:50;i:2;i:100;}i:1;a:3:{i:0;i:10;i:1;i:50;i:2;i:100;}}', 'a:2:{i:0;a:3:{i:0;i:10;i:1;i:50;i:2;i:100;}i:1;a:3:{i:0;i:10;i:1;i:50;i:2;i:100;}}', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(27, NULL, 'status_notification', '1', '1', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(28, NULL, 'comment_notification', '1', '1', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(29, NULL, 'queue_emails', '0', '0', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(30, NULL, 'assigned_notification', '1', '1', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(31, NULL, 'agent_restrict', '0', '0', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(32, NULL, 'close_ticket_perm', 'a:3:{s:5:\"owner\";b:1;s:5:\"agent\";b:1;s:5:\"admin\";b:1;}', 'a:3:{s:5:\"owner\";b:1;s:5:\"agent\";b:1;s:5:\"admin\";b:1;}', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(33, NULL, 'reopen_ticket_perm', 'a:3:{s:5:\"owner\";b:1;s:5:\"agent\";b:1;s:5:\"admin\";b:1;}', 'a:3:{s:5:\"owner\";b:1;s:5:\"agent\";b:1;s:5:\"admin\";b:1;}', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(34, NULL, 'delete_modal_type', 'builtin', 'builtin', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(35, NULL, 'editor_enabled', '1', '1', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(36, NULL, 'include_font_awesome', '1', '1', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(37, NULL, 'summernote_locale', 'it', 'en', '2019-05-30 09:55:09', '2019-05-30 10:28:12'),
(38, NULL, 'editor_html_highlighter', '1', '1', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(39, NULL, 'codemirror_theme', 'monokai', 'monokai', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(40, NULL, 'summernote_options_json_file', 'vendor/kordy/ticketit/src/JSON/summernote_init.json', 'vendor/kordy/ticketit/src/JSON/summernote_init.json', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(41, NULL, 'purifier_config', 'a:3:{s:15:\"HTML.SafeIframe\";s:4:\"true\";s:20:\"URI.SafeIframeRegexp\";s:72:\"%^(http://|https://|//)(www.youtube.com/embed/|player.vimeo.com/video/)%\";s:18:\"URI.AllowedSchemes\";a:5:{s:4:\"data\";b:1;s:4:\"http\";b:1;s:5:\"https\";b:1;s:6:\"mailto\";b:1;s:3:\"ftp\";b:1;}}', 'a:3:{s:15:\"HTML.SafeIframe\";s:4:\"true\";s:20:\"URI.SafeIframeRegexp\";s:72:\"%^(http://|https://|//)(www.youtube.com/embed/|player.vimeo.com/video/)%\";s:18:\"URI.AllowedSchemes\";a:5:{s:4:\"data\";b:1;s:4:\"http\";b:1;s:5:\"https\";b:1;s:6:\"mailto\";b:1;s:3:\"ftp\";b:1;}}', '2019-05-30 09:55:09', '2019-05-30 09:55:09'),
(42, NULL, 'routes', 'C:\\Users\\enry\\Desktop\\Progetti 2019\\Tutorial lavarel\\Sistema ticket laravel\\ticketit\\vendor/kordy/ticketit/src/routes.php', 'C:\\Users\\enry\\Desktop\\Progetti 2019\\Tutorial lavarel\\Sistema ticket laravel\\ticketit\\vendor/kordy/ticketit/src/routes.php', '2019-05-30 09:55:09', '2019-05-30 09:55:09');

-- --------------------------------------------------------

--
-- Struttura della tabella `ticketit_statuses`
--

CREATE TABLE `ticketit_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `ticketit_statuses`
--

INSERT INTO `ticketit_statuses` (`id`, `name`, `color`) VALUES
(1, 'Nuovo', '#e9551e'),
(2, 'Chiuso', '#186107'),
(3, 'Riaperto', '#71001f');

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ticketit_admin` tinyint(1) NOT NULL DEFAULT '0',
  `ticketit_agent` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `ticketit_admin`, `ticketit_agent`) VALUES
(1, 'alessandro', 'iannacone.alessandro@gmail.com', NULL, '$2y$10$6qBcgaJiSz6LX1/OUnIlAecUa01zBtUQhv1R4HdJN5pXBKxSd5fJi', 'fiU9bbLXTDjmNtZjkpMbZPXEFeAClvNl5JVVugymRVpcMSgF92wDX3CN8ogx', '2019-05-30 09:55:07', '2019-05-30 10:27:48', 1, 1),
(2, 'alessandro', 'ianna@live.it', NULL, '$2y$10$d5Em6OqfYQYzD7BNtQsj9epOaPxJYEMJjrUfO68y2FyJGEj6/bGnC', 'LPzm8XIFeW4pEov8fgcdIe1dKZJEkCk8zmVBPM382LQFCkifUjtYhM10HmK2', '2019-05-30 09:57:36', '2019-05-30 09:57:36', 0, 0),
(3, 'geniushoroscope.com', 'brando.trionfera@gmail.com', NULL, '$2y$10$RtRFedNzeluGhij7KBhQxuH/EaCVNfX/pGl1tk1mS0B3aQ4NtQtwe', '7xJkHJVxYdqo2K5A6Ublv0VZT1G8W7ZGnaYcyY2eQOUPfLiDncweQcXCKpME', '2019-05-30 12:17:43', '2019-05-30 12:17:43', 0, 0);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `abbonamentomodels`
--
ALTER TABLE `abbonamentomodels`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indici per le tabelle `ticketit`
--
ALTER TABLE `ticketit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ticketit_subject_index` (`subject`),
  ADD KEY `ticketit_status_id_index` (`status_id`),
  ADD KEY `ticketit_priority_id_index` (`priority_id`),
  ADD KEY `ticketit_user_id_index` (`user_id`),
  ADD KEY `ticketit_agent_id_index` (`agent_id`),
  ADD KEY `ticketit_category_id_index` (`category_id`),
  ADD KEY `ticketit_completed_at_index` (`completed_at`);

--
-- Indici per le tabelle `ticketit_audits`
--
ALTER TABLE `ticketit_audits`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `ticketit_categories`
--
ALTER TABLE `ticketit_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `ticketit_comments`
--
ALTER TABLE `ticketit_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ticketit_comments_user_id_index` (`user_id`),
  ADD KEY `ticketit_comments_ticket_id_index` (`ticket_id`);

--
-- Indici per le tabelle `ticketit_priorities`
--
ALTER TABLE `ticketit_priorities`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `ticketit_settings`
--
ALTER TABLE `ticketit_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ticketit_settings_slug_unique` (`slug`),
  ADD UNIQUE KEY `ticketit_settings_lang_unique` (`lang`),
  ADD KEY `ticketit_settings_lang_index` (`lang`),
  ADD KEY `ticketit_settings_slug_index` (`slug`);

--
-- Indici per le tabelle `ticketit_statuses`
--
ALTER TABLE `ticketit_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `abbonamentomodels`
--
ALTER TABLE `abbonamentomodels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT per la tabella `ticketit`
--
ALTER TABLE `ticketit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `ticketit_audits`
--
ALTER TABLE `ticketit_audits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `ticketit_categories`
--
ALTER TABLE `ticketit_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `ticketit_comments`
--
ALTER TABLE `ticketit_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `ticketit_priorities`
--
ALTER TABLE `ticketit_priorities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `ticketit_settings`
--
ALTER TABLE `ticketit_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT per la tabella `ticketit_statuses`
--
ALTER TABLE `ticketit_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
